/*
 * Copyright 2018, Richard Sharpe
 */

#include <sys/types.h>
#include <stdbool.h>
#include <stdint.h>

/*
 * The layer that the pkt generation code operates at.
 */
enum pkt_gen_t {
     NETWORK_ACCESS_LAYER,
     NETWORK_LAYER,
     TRANSPORT_LAYER,
     APPLICATION_LAYER
};

/*
 * Each struct can be handled as an array of elements. We know how many elements
 * there are because even arrays are an element. Even a switch with a void 
 * entry is an element.
 *
 * At the point where we are assembling the elements of the array we are dealing
 * with flattened structures as well.
 *
 * There are two main types of elements:
 *    * Immediate. They are in the bytes of the elt and limited to 8/16 bytes.
 *    * Indirect. They are pointed to by the elt and contain a header?
 *
 * An indirect type just has a pointer to the header. Header needed because
 * it might be recursive.
 *
 * An immediate type has a length.
 */
enum elt_type {
	ELT_IMMEDIATE,
	ELT_INDIRECT,       /* Struct */
	ELT_INDIRECT_DATA   /* Pure data, len set by len */
};

/*
 * An array of elts for a struct ...
 */
struct struct_elt {
	enum elt_type type; /* Our type, indirect */
	uint32_t len;       /* Total byte len     */
	uint32_t count;     /* Count of elts   */
	uint32_t next_elt;  /* Next elt to add */
	void * elements[1]; /* The elements    */
};

struct element {
	enum elt_type type;
        uint32_t len;      /* Length of the elt, including 0 for void. */
        union {
          void *indirect;
          uint8_t data[16];
        } d;
};

struct indirect_elt {
	enum elt_type type;
	uint32_t len;
	uint8_t data[1];
};

struct value_elt_32 {
	uint32_t min;
	uint32_t max;
	bool valid;
};

struct value_elts_32 {
	uint32_t elt_count;
	struct value_elt_32 *elts;
	uint32_t state;
	uint32_t value;
};

#define ELT_ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))

/*
 * The callback that supplies a packet.
 *
 * @return int Whether or not everything is OK and generation should continue
 *             0 means all OK, > 0 indicates to cease generating packets.
 *
 * @param int len, the length of the packet being returned.
 *
 * @param uint8_t *pkt, the packet data.
 *
 * This callback must accept the packet, prepend the necessary headers and
 * then write the packet out.
 */
struct params;

typedef int (*pkt_gen_pkt_callback_t)(struct params *params,
				      enum pkt_gen_t layer,
				      struct struct_elt *pkt);

/*
 * The params structure.
 */
struct params {
	char *output_file;
	uint8_t src_mac[6];
	uint8_t dst_mac[6];
	uint8_t ether_type[2];
	uint8_t src_ip[4];
	uint8_t dst_ip[4];
	uint8_t src_port[2];
	uint8_t dst_port[2];
	uint32_t session_count;
	uint32_t packet_count;  /* Per session */
	bool gen_short_pkts;
	bool gen_long_pkts;
	bool gen_error_vals;
	bool error_fraction;
	pkt_gen_pkt_callback_t pkt_gen_pkt_callback;
};

/*
 * The pkt_gen routines
 */
extern int pkt_gen_init(struct params *params, void **ctx);
extern int pkt_gen_startup(struct params *params, void *ctx);
extern int pkt_gen_body(struct params *params, void *ctx);
extern int pkt_gen_shutdown(struct params *params, void *ctx);
extern void pkt_gen_close(struct params *params, void *ctx);
extern char *pkt_gen_geterr(struct params *params, void *ctx);
