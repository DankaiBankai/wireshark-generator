/*
 * A generic packet generator application.
 *
 * Copyright Richard Sharpe, 2018.
 *
 */

#include "pkt-gen.h"

#include <errno.h>
#include <pcap/pcap.h>
#include <stdlib.h>
#include <string.h>

/*
 * Packets are passed to us as a linked list of fragments. There is an API for this.
 *
 *  add frag adds a fragment to the end and updates the length.
 */
struct packet_struct {
	int len;      /* The overall len */
	struct packet_frag *head;
	struct packet_frag *tail;
};

struct packet_frag {
	struct packet_frag *next;
	struct packet_frag *prev; /* Not sure we need this */
	int len;
	uint32_t buf[1];          /* Will actually be len bytes long */
};

struct packet_frag *get_pkt_frag()
{

}

void add_pkt_frag(struct packet_struct *packet, struct packet_frag *frag)
{

}

/*
 * Move through the packet serializing it.
 */
void struct_serialize(struct params *params, struct struct_elt *pkt,
		      uint8_t buffer[])
{
	uint32_t offset = 0;
	uint16_t i;

	for (i = 0; i < pkt->count; i++) {
		struct struct_elt* elt = pkt->elements[i];

		switch (elt->type) {
		case ELT_IMMEDIATE:
			memcpy(&buffer[offset], ((struct element *)elt)->d.data,
				((struct element *)elt)->len);
			offset += ((struct element *)elt)->len;
			break;
		case ELT_INDIRECT:
			struct_serialize(params, elt, &buffer[offset]);
			offset += elt->len;
			break;
		case ELT_INDIRECT_DATA:
			memcpy(&buffer[offset],
				((struct indirect_elt *)elt)->data,
				((struct indirect_elt *)elt)->len);
			offset += elt->len;
			break;
		}
	}
}

/*
 * Serialize each element in the pkt given to us. Her we just fill in the
 * required headers ahead of the pkt and call struct_serialize.
 *
 * The buffer needs to be of the correct size.
 */
void pkt_serialize(struct params *params, enum pkt_gen_t layer,
		   struct struct_elt *pkt, uint8_t buffer[])
{
	/*
	 * Eventually we need to handle more headers, but just assume
	 * the ethernet header for now.
	 */
	memcpy(&buffer[0], params->dst_mac, 6);
	memcpy(&buffer[6], params->src_mac, 6);
	memcpy(&buffer[12], params->ether_type, 2);

	struct_serialize(params, pkt, &buffer[14]);
}

pcap_dumper_t *dumper = NULL;

/*
 * Construct the whole packet by using the pre-generated pieces and appending
 * the new piece ...
 */
int pkt_gen_pkt_callback(struct params *params, enum pkt_gen_t layer,
			 struct struct_elt *pkt) {
	uint8_t *complete_pkt;
	struct pcap_pkthdr hdr;
	uint32_t pkt_len;
	struct timeval ts;

	/*
	 * Build the complete packet from what we were handed ...
	 */
	printf("Given a packet of size %u ...\n", pkt->len);

	/*
	 * Figure out the length of buffer we need, including the bits
	 * we have to add.
	 *
	 * For now, just add an Ethernet header.
	 */
	pkt_len = pkt->len + 14;
	complete_pkt = malloc(pkt_len);
	if (complete_pkt == NULL) {
		printf("Unable to allocate %u bytes of memory!\n", pkt_len);
		return -ENOMEM;
	}

	/*
	 * Add the header and then serialize the data
	 */
	pkt_serialize(params, layer, pkt, complete_pkt);

	/*
	 * Now, dump it.
	 */
	gettimeofday(&ts, NULL);
	hdr.ts = ts;
	hdr.caplen = pkt_len;
	hdr.len = pkt_len;
	pcap_dump((u_char *)dumper, &hdr, (u_char *)complete_pkt);

	return 0;
}

static uint8_t def_src_mac[] = { 0x00, 0x50, 0x56, 0x8c, 0xaf, 0xee };
static uint8_t def_dst_mac[] = { 0x00, 0x50, 0x56, 0x8c, 0xe8, 0xd2 };
static char *def_out_file = "generated.pcap";

void parse_params(struct params *params)
{
	if (params == NULL) {
		printf("No params struct passed!");
		exit(1);
	}

	memset(params, 0, sizeof(*params));

	/*
	 * Just fill params with pre-set info for now.
	 */
	memcpy(&params->src_mac, def_src_mac, 6);
	memcpy(&params->dst_mac, def_dst_mac, 6);
	params->session_count = 1;
	params->packet_count = 18;
	params->output_file = def_out_file;
	params->pkt_gen_pkt_callback = pkt_gen_pkt_callback;
}

int main(int argc, char *argv[])
{
	int err = -1;
	pcap_t *pd = NULL;
	void *ctx = NULL;
	struct params params;

	/*
	 * Parse the parameters
	 */
	parse_params(&params);
	/*
	 * Now open the dump file
	 */
	pd = pcap_open_dead(DLT_EN10MB, 65535);
	if (pd == NULL) {
		fprintf(stderr, "Unable to open pcap device: %s\n",
			strerror(errno));
		return -1;
	}

	dumper = pcap_dump_open(pd, params.output_file);
        if (dumper == NULL) {
		fprintf(stderr, "Unable to create dump file %s: %s\n",
			params.output_file, pcap_geterr(pd));
		goto close_pd;
	}

	/*
	 * Now, initialize the pkt_gen module
	 */
	if (pkt_gen_init(&params, &ctx) != 0) {
		fprintf(stderr, "Unable to initialize pkt gen lib: %s\n",
			pkt_gen_geterr(&params, &ctx));
		goto close_dumper;
	}

	/*
	 * Now, run the pkt_gen startup, ie session setup if needed.
	 */
	if (pkt_gen_startup(&params, ctx) != 0) {
		fprintf(stderr, "Unable to run pkt gen startup: %s\n",
			pkt_gen_geterr(&params, &ctx));
		goto close_dumper;
	}

	/*
	 * Now, generate the body
	 */
	if (pkt_gen_body(&params, ctx) != 0) {
		fprintf(stderr, "Unable to run the pkt gen body: %s\n",
			pkt_gen_geterr(&params, &ctx));
		goto close_pkt_gen;
	}

	/*
	 * Now, run the shutdown, ie session shutdown if needed.
	 */
	if (pkt_gen_shutdown(&params, ctx) != 0) {
		fprintf(stderr, "Unable to run the pkt gen shutdown: %s\n",
			pkt_gen_geterr(&params, &ctx));
		goto close_pkt_gen;
	}

close_pkt_gen:
	pkt_gen_close(&params, &ctx);
close_dumper:
	pcap_dump_close(dumper);
close_pd:
	pcap_close(pd);
	return err;
}
