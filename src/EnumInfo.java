/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

// How we manage information about enums.
public class EnumInfo {
    String name;
    String type;
    int definingLine;
    int maxIdLen;   // Longest ID
    int bitLen;     // Length in bits.

    TreeSet<EnumElt> elts = new TreeSet<EnumElt>();

    Boolean hasDefault;
    String defaultString;  // If there is one

    public EnumInfo() {
        hasDefault = false;
        maxIdLen = 0;
    }

    public void updateMaxIdLen(int len) {
        if (len > maxIdLen)
            maxIdLen = len;
    }

    public void addEnumElt(String eVal, String eId, String eString) {
        EnumElt elt = new EnumElt(eVal, eId, eString);
        elts.add(elt);
    }

    String getType() {
        return type;
    }

    Boolean isBaseType() {
        return Util.isBaseType(type);
    }

    // Doesn't need to check up the stack
    Boolean checkMissing() {
        return !Util.isBaseType(type);
    }

    // Get the extra display info needed
    String getWsDisplayInfo() {
        if (!type.equals("bit") && !type.equals("boolean"))
            return "|BASE_RANGE_STRING";
        return "";
    }

    // Figure out the field convert needed
    String getWsFieldConvert() {
        if (type.equals("bit") || type.equals("boolean"))
            return "TFS(&" + name + "_tfs)";
        else
            return "RVALS(" + name + "_rvals)";
    }

    // Generate a binary enum, with a true_false_string for the
    // hf arrays.
    void generateBinaryEnum() {
        ArrayList<String> val_strings = new ArrayList<String>();

        System.out.println("enum " + name + " {");
        String val_string;

        // There will only be two elements, won't there!
        elts.first().generateEnumDecl(maxIdLen);
        val_string = elts.first().generateTfsValString();
        val_strings.add(val_string);
        elts.last().generateEnumDecl(maxIdLen);
        val_string = elts.last().generateTfsValString();
        val_strings.add(val_string);
        System.out.println("};\n");

        // Now iterate the val_strings in reverse order to
        // generate the tfs.
        if (val_strings.size() > 0) {
            System.out.println("static const true_false_string " +
                               name + "_tfs = {");
            System.out.println(val_strings.get(1));
            System.out.println(val_strings.get(0));
            System.out.println("};\n");
        }
    }

    // Generate a normal enum.
    void generateNonBinaryEnum() {
        ArrayList<String> val_strings = new ArrayList<String>();
        int maxVal = (1 << bitLen) - 1;
        int prevVal = 0;
        String defStr;

        if (hasDefault)
            defStr = defaultString;
        else
            defStr = "unknown";

        // First the defines
        System.out.println("enum " + name + " {");
        for (EnumElt elt:elts) {
            String val_string;

            elt.generateEnumDecl(maxIdLen);

            val_string = elt.generateRangeString(prevVal, defStr);
            if (val_string != null) {
                val_strings.add(val_string);
            }
            prevVal = Integer.decode(elt.val);
        }

        // If there is a left-over set of values, add one more.
        if (prevVal != maxVal) {
            String val_string;

            val_string = "  { " + (prevVal + 1) + ", " + maxVal +
                         ", " + defStr + " }";
            val_strings.add(val_string);
        }

        // Do the last bit.
        System.out.println("};\n");

        // Now the value_string decls, if any
        if (val_strings.size() > 0) {
            System.out.println("static const range_string " +
                               name + "_rvals[] = {");
            for (String line:val_strings) {
                System.out.println(line);
            }
            System.out.println("};\n");
        }
    }

    // Generate defines and any value_strings needed.
    void generateEnumDecl() {
        if (elts.size() == 0)
            return;

        // Want to generate different string arrays, like range_string
        // or true_false strings etc, depending on type.
        if (type.equals("bit") || type.equals("boolean"))
            generateBinaryEnum();
        else
            generateNonBinaryEnum();

    }
}
