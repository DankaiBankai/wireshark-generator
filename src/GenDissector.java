/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.*;

// A simple code generating class that handles generating C code.
class CodeGen {

    // Quote a name. Strip any quotes and add quotes.
    private static String quoteString(String str) {
        return "\"" + str.replaceAll("\"","") + "\"";
    }

    // Generate an HF array line. The Strings will be quoted where needed
    static void genHfEntry(String name, String label, String searchString,
                           String type, String disp, String special,
                           String mask, String desc, String extra) {
        System.out.println("    {&" + name + ",");
        System.out.println("     {" + quoteString(label) + ", " +
                           quoteString(searchString) + ",");
        System.out.println("      " + type + ", " + disp + ", " +
                           (special == null ? "NULL" : special) +
                           ", " + mask + ", " +
                           (desc == null ? "NULL" : quoteString(desc)) + ", " +
                           extra + " }},\n");
    }
}

public class GenDissector {

    CodeGenType codeGenType;

    private static Generator gen = null;

    private Boolean syntaxError = false;

    public GenDissector(CodeGenType type) {
        codeGenType = type;
    }

    public static Generator getGenerator() {
        return gen;
    }

    public void setSyntaxError() {
        syntaxError = true;
    }

    /*
     * We cannot generate the code for a struct until we have seen the
     * entire struct. This is because later elements can depend on earlier
     * elements, in which case we need to fetch the fields that are depended on
     * into local variables rather than simply inserting proto_tree_add_item
     * statements.
     *
     * We also have to save structures in a symbol table because we may need
     * to refer to them in other structures.
     *
     * Whenever we refer to an external table we also have to save the
     * dissector table that contains it.
     */

    // We need an instance of a Generator because we need to allocate
    // objects later.
    public void doGen(InputStream is, String inputFile, CodeGenType codeType) throws Exception {

        ANTLRInputStream input = new ANTLRInputStream(is);
        WiresharkGeneratorLexer lexer = new WiresharkGeneratorLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        WiresharkGeneratorParser parser = new WiresharkGeneratorParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new GeneratorErrorListener());

        ParseTree tree = parser.protocol();     // parse

        if (syntaxError)
            return;

        ParseTreeWalker walker = new ParseTreeWalker();

        switch (codeType) {
        case LUA_CODE_GEN:
            gen = new LuaGenerator(inputFile);
            break;
        case PKT_CODE_GEN:
            gen = new PktGenerator(inputFile);
            break;
        default:
            gen = new Generator(inputFile);
            break;
        }

        gen.setParser(parser);

        walker.walk(gen, tree);

    }

    static GenDissector genDis;

    public static GenDissector getGenDis() {
        return genDis;
    }

    static String inputFile = null;

    public static String getInputFilename() {
        return inputFile;
    }

    public static void main(String[] args) throws Exception {
        int argNum = 0;
        CodeGenType codeGenType = CodeGenType.C_CODE_GEN;

        // Process the args
        while (args.length > 0) {
            if (args[argNum].equals("-d")) {
                argNum++;

                if (args.length < 2) {
                    System.out.println("Invalid command line. -d takes a " +
                                      "parameter");
                    return;
                }

                Debug.setDebugLevel(Integer.parseInt(args[argNum]));
                argNum++;
            } else if (args[argNum].equals("-l")) {
                argNum++;
                codeGenType = CodeGenType.LUA_CODE_GEN;
            } else if (args[argNum].equals("-p")) {
                argNum++;
                codeGenType = CodeGenType.PKT_CODE_GEN;
            } else {
                break; //Nothing here for us
            }
        }

        //System.out.println("//argNum: " + argNum + " = " + args[argNum]);
        if (args.length > argNum) {
            inputFile = args[argNum];
        }

        InputStream is = System.in;

        if (inputFile != null) {
            try {
                is = new FileInputStream(inputFile);
            }
            catch (FileNotFoundException e) {
                System.out.println("File \"" + inputFile + "\" not found!");
                System.exit(1);
            }
            catch (Exception e) {
                System.out.println("Exception \"" + e.getMessage() +
                                   "\" trying to open file \"" + inputFile + "\"");
                System.exit(2);
            }
        } else {
            inputFile = "stdin";
        }

        // We need an instance of this object ...
        genDis = new GenDissector(codeGenType);

	genDis.doGen(is, inputFile, codeGenType);
    }
}
