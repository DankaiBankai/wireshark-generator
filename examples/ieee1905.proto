# Set the endianness ...
endian = big ; # Default = big

enum message_version_enum:uint8 {
  0x00 = VERSION_1: "Version 1",
  default = "Reserved"
};

enum last_fragment_enum:boolean {
  0x0 = NOT_LAST_FRAG: "Not the last fragment",
  0x1 = LAST_FRAG:     "The last fragment"
};

enum relay_indicator_enum:bit {
  0x0 = NOT_RELAYED: "Not to be relayed",
  0x1 = RELAYED:     "Shall be relayed"
};

enum message_type_enum:uint16 {
  0x0000 = TOPOLOGY_DISCOVERY:    "Topology discovery",
  0x0001 = TOPOLOGY_NOTIFICATION: "Topology notification",
  0x0002 = TOPOLOGY_QUERY:        "Topology query",
  0x0003 = TOPOLOGY_RESPONSE:     "Topology response",
  0x0004 = VENDOR_SPECIFIC:       "Vendor specific",
  0x0005 = LINK_METRIC_QUERY:     "Link metric query",
  0x0006 = LINK_METRIC_RESPONSE:  "Link metric response",
  0x0007 = AP_AUTOCONFIGURATION_SEARCH: "AP-autoconfiguration search",
  0x0008 = AP_AUTOCONFIGURATION_RESPONSE: "AP-autoconfiguration response",
  0x0009 = AP_AUTOCONFIGURATION_WSC: "AP-autoconfiguration WSC",
  0x000A = AP_AUTOCONFIGURATION_RENEW: "AP-autoconfiguration renew",
  0x000B = IEEE1905_PUSH_BUTTON_EVENT_NOTIF: "1905 push button event notification",
  0x000C = IEEE1905_PUSH_BUTTON_JOIN_NOTIF: "1905 push button join notification",
  0x000D = HIGHER_LAYER_QUERY: "Higher Layer query",
  0x000E = HIGHER_LAYER_RESPONSE: "Higher Layer response",
  0x000F = INTERFACE_POWER_CHANGE_REQ: "Interface Power Change Request",
  0x0010 = INTERFACE_POWER_CHANGE_RSP: "Interface Power Change Response",
  0x0011 = GENERIC_PHY_QUERY: "Generic Phy query",
  0x0012 = GENERIC_PHY_RSP: "Generic Phy response",
  default = "Reserved"
};

struct ieee1905_al_mac_address_info {
  ether_t "1905 AL MAC address of transmitting device";
};

struct ieee1905_mac_address_info {
  ether_t "MAC address of transmitting interface";
};

enum media_type:uint16 {
  0x0000 = FAST_ENET: "IEEE 802.3u fast Ethernet",
  0x0001 = GB_ENET:   "IEEE 802.3ab gigabit Ethernet",
  0x0100 = WIFI_B:    "IEEE 802.11b (2.4 GHz)",
  0x0101 = WIFI_G:    "IEEE 802.11g (2.4 GHz)",
  0x0102 = WIFI_A5:   "IEEE 802.11a (5GHz)",
  0x0103 = WIFI_A24:  "IEEE 802.11a (2.4GHz)",
  0x0104 = WIFI_N5:   "IEEE 802.11n (5GHz)",
  0x0105 = WIFI_N24:  "IEEE 802.11n (2.4GHz)",
  0x0106 = WIFI_AD:   "IEEE 802.11ad (60GHz)",
  0x0107 = WIFI_AF:   "IEEE 802.11af (whitespace)",
  0x0200 = IEEE1901W: "IEEE 1901 wavelet",
  0x0201 = IEEE1901F: "IEEE 1901 FFT",
  0x0300 = MOCA11:    "MoCA v1.1",
  default = "Reserved"
};

struct local_interface_details {
  ether_t "Local interface";
  media_type "Media type";
  uint8 "Media specific info len";
  byte "Media specific information"["Media specific info len"];
};

struct ieee1905_device_information {
  ether_t "1905 AL MAC address of device";
  uint8 "Local interface count";
  local_interface_details "Local interface list"["Local interface count"];
};

enum channel_preference_prefs:uint4 { # Do we need the uint8
  0x0 = NON_OPERABLE:    "Non-operable",
  0x1 = OPERABLE_PREF_1: "Operable with preferance score 1",
  0x2 = OPERABLE_PREF_2: "Operable with preference score 2",
  default = "Reserved"
};

enum channel_preference_reason:uint4 {
  0x0 = UNSPECIFIED:          "Unspecified",
  0x1 = PROXIMATE_INTERFERER:
            "Proximate non-802.11 interferer in local environment",
  default = "Reserved"
};

struct channel_preference_flags {
  uint8:7-4:channel_preference_prefs preference,
       :3-0:channel_preference_reason reason;
};

struct channel_preference_detail {
  uint8 "Operating class";
  uint8 "Channel list count";
  uint8 "Channel list"["Channel list count"];
  channel_preference_flags "Flags";
};

typedef byte radio_id[6];

typedef byte bssid[6];

struct ieee1905_channel_preference {
  radio_id "Radio unique identifier";
  uint8 "Operating classes";
  channel_preference_detail "Operating class list"["Operating classes"];
};

enum tlv_type_enum:uint8 {
  0 = IEEE1905_END_OF_MESSAGE_TLV:          "End of message",
  1 = IEEE1905_AL_MAC_ADDRESS_TYPE_TLV:     "1905 AL MAC address type",
  2 = IEEE1905_MAC_ADDRESS_TYPE_TLV:        "MAC address type",
  3 = IEEE1905_DEVICE_INFORMATION_TYPE_TLV: "Device information type",
  4 = IEEE1905_DEVICE_BRIDGING_CAPABILITY_TLV: "Device bridging capability",
  6 = IEEE1905_NON1905_NEIGHBOR_DEVICE_LIST_TLV: "Non-1905 neighbor device list",
  7 = IEEE1905_1905_NEIGHBOR_DEVICE_LIST_TLV: "1905 neighbor device",
  8 = IEEE1905_LINK_METRIC_QUERY_TLV:       "Link metric query",
  9 = IEEE1905_TRANSMITTER_LINK_METRIC_TLV: "1905 transmitter link metric",
  10 = IEEE1905_RECEIVER_LINK_METRIC_TLV:   "1905 receiver link metric",
  11 = IEEE1905_VENDOR_SPECIFIC_TLV:        "Vendor specific",

  0x8B = IEEE1905_CHANNEL_PREFERENCE_TLV:   "Channel Preference",
  0x9B = IEEE1905_STEERING_REQUEST_TLV:     "Steering Request",
  0x9C = IEEE1905_STEERING_BTM_REPORT_TLV:  "Steering BTM Report",

  default = "Reserved"
};

struct ieee1905_vendor_specific {
  oui_t "Vendor specific OUI";
  byte "Vendor specific information"[../tlv_header/tlvLength - 3];
};

enum steering_request_mode:boolean {
  0 = REQUEST_IS_STEERING_OPPORTUNITY: "Request is a steering opportunity",
  1 = REQUEST_IS_STEERING_MANDATE:      "Request is a steering mandate"
};

struct steering_request_flags {
  uint8:7:steering_request_mode "Request Mode",
       :6:boolean "BTM Disassociation Imminent",
       :5:boolean "BTM Abridged",
       :4-0:uint8 "Reserved";
};

struct steering_op_window {
  uint16 "Steering opportunity window";
};

struct target_bssid_info {
  bssid "Target BSSID";
  uint8 "Target BSSID Operating Class";
  uint8 "Target BSSID Channel Number";
};

struct ieee1905_steering_request {
  bssid "Source BSS BSSID";
  steering_request_flags "Steering request flags";
  switch ("Steering request flags"/"Request Mode") {
    case REQUEST_IS_STEERING_OPPORTUNITY:
      void;
    case REQUEST_IS_STEERING_MANDATE:
      steering_op_window;
  };
  uint16 "BTM disassociation timer";
  uint8 "STA list count";
  ether_t "Target MAC address"["STA list count"];
  uint8 "Target BSSID List Count";
  target_bssid_info "Target BSSID List"["Target BSSID List Count"];
};

struct ieee1905_steering_btm_report {
  bssid "Report BSSID";
  ether_t "Reported STA MAC address";
  uint8 "BTM Status Code";
  switch (../tlv_header/tlvLength - 13) {
    case 6:
      bssid "Target BSSID";
    case 0:
      void;
    default:
      exception(../tlv_header/tlvLength, incorrect_length, PI_PROTOCOL, PI_ERROR, "Malformed Steering BTM Report, len should be 13 or 19");
  };
};

struct unknown_tlv_type {
    byte "Unknown TLV data"[../tlv_header/tlvLength];
};

struct tlv_header_type {
  tlv_type_enum tlvType;
  uint16 tlvLength;
};

struct protocol_tlv {
  tlv_header_type tlv_header;
  switch (tlv_header/tlvType) {
    case IEEE1905_END_OF_MESSAGE_TLV:
      void;

    case IEEE1905_AL_MAC_ADDRESS_TYPE_TLV:
      ieee1905_al_mac_address_info "1905 AL MAC address type";

    case IEEE1905_MAC_ADDRESS_TYPE_TLV:
      ieee1905_mac_address_info "1905 MAC address type";

    case IEEE1905_DEVICE_INFORMATION_TYPE_TLV:
      ieee1905_device_information "1905 Device Information";

    case IEEE1905_VENDOR_SPECIFIC_TLV:
      ieee1905_vendor_specific "Vendor Specific";

    case IEEE1905_CHANNEL_PREFERENCE_TLV:
      ieee1905_channel_preference;

    case IEEE1905_STEERING_REQUEST_TLV:
      ieee1905_steering_request;

    case IEEE1905_STEERING_BTM_REPORT_TLV:
      ieee1905_steering_btm_report;

    default:
      unknown_tlv_type;
  };
};

struct ieee1905_cmdu {
  message_version_enum messageVersion;
  uint8 reserved;
  message_type_enum messageType;
  uint16 messageId;
  uint8 fragmentId;
  uint8:7:last_fragment_enum lastFragmentIndicator,
       :6:relay_indicator_enum relayIndicator,
       :5-0:uint8 reserved;
  protocol_tlv ProtocolTlvs[ProtocolTlvs/tlv_header/tlvType != 0];
  protocol_tlv "End Of Mesage";
};

protoDetails = { "IEEE 1905.1a", "ieee1905", "ieee1905" };

dissectorEntry ieee1905 = ieee1905_cmdu;

dissectorTable["ethertype", "0x893A"] = ieee1905;
